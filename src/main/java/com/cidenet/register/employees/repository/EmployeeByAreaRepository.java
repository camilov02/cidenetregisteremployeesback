package com.cidenet.register.employees.repository;

import com.cidenet.register.employees.domain.entities.EmployeeByAreaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface EmployeeByAreaRepository extends JpaRepository<EmployeeByAreaEntity, Long> {

}
