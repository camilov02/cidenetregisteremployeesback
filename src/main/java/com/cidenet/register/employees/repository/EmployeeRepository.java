package com.cidenet.register.employees.repository;

import com.cidenet.register.employees.domain.entities.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional()
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

    @Query(value = "SELECT * FROM employee e WHERE e.email LIKE %:email% ORDER BY e.id DESC LIMIT 1", nativeQuery = true)
    public EmployeeEntity findEmployeeByEmailTop(@Param("email") String email);

    public EmployeeEntity findByIdentification(String identification);
    public EmployeeEntity findByEmail(String email);
    public Optional<EmployeeEntity> findById(Long id);
    public List<EmployeeEntity> findAllByOrderByIdDesc();
}
