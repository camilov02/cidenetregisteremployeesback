package com.cidenet.register.employees.repository;

import com.cidenet.register.employees.domain.entities.IdentificationTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional()
public interface IdentificationTypeRepository extends JpaRepository<IdentificationTypeEntity, Long> {

    public Optional<IdentificationTypeEntity> findById(Long id);
}
