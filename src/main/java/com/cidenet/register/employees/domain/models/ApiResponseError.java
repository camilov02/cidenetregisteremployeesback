package com.cidenet.register.employees.domain.models;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ApiResponseError {

    private HttpStatus httpStatus;
    private List<String> listErrors;

    public ApiResponseError(HttpStatus httpStatus, List<String> listErrors) {
        this.httpStatus = httpStatus;
        this.listErrors = listErrors;
    }

    public ApiResponseError getApiResponse() {
        ApiResponseError apiResponseError = new ApiResponseError(httpStatus, listErrors);
        return apiResponseError;
    }
}
