package com.cidenet.register.employees.domain.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "employee")
public class EmployeeEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "{employee.firstLastName.empty}")
    @Size(min = 3, max = 20, message = "{employee.firstLastName.size}")
    @Pattern(regexp = "[a-zA-Z ]+", message = "{employee.firstLastName.pattern}")
    private String firstLastName;

    @NotEmpty(message = "{employee.secondLastName.empty}")
    @Size(min = 3, max = 20, message = "{employee.secondLastName.size}")
    @Pattern(regexp = "[a-zA-Z ]+", message = "{employee.secondLastName.pattern}")
    private String secondLastName;

    @NotEmpty(message = "{employee.firstName.empty}")
    @Size(min = 3, max = 20, message = "{employee.firstName.size}")
    @Pattern(regexp = "[a-zA-Z ]+", message = "{employee.firstName.pattern}")
    private String firstName;

    @Size(min = 3, max = 50, message = "{employee.secondName.size}")
    @Pattern(regexp = "[a-zA-Z ]+", message = "{employee.secondName.pattern}")
    private String secondName;

    @NotEmpty(message = "{employee.identification.empty}")
    @Size(min = 3, max = 20, message = "{employee.identification.size}")
    @Pattern(regexp = "[a-zA-Z0-9]+", message = "{employee.identification.pattern}")
    @Column(unique = true)
    private String identification;

    @Size(min = 10, max = 300, message = "{employee.email.size}")
    @Pattern(regexp = "^[a-zA-Z0-9.@-]*$", message = "{employee.email.pattern}")
    @Column(unique = true)
    private String email;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "area_id", referencedColumnName = "id")
    private EmployeeByAreaEntity employeeArea;

    @JoinColumn(name = "country_id", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private CountryEntity country;

    @JoinColumn(name = "identification_type_id", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private IdentificationTypeEntity identificationType;

}
