package com.cidenet.register.employees.adapter;

import com.cidenet.register.employees.domain.entities.EmployeeEntity;
import com.cidenet.register.employees.domain.entities.IdentificationTypeEntity;
import com.cidenet.register.employees.services.IIdentificationTypeServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/identification-type")
public class IdentificationTypeAdapter {

    @Autowired
    private IIdentificationTypeServices identificationTypeServices;

    @GetMapping("/list")
    public ResponseEntity<?> listIdentificationType() {
        List<IdentificationTypeEntity> listIdentificationType = identificationTypeServices.findAll();
        if (listIdentificationType != null) {
            if (listIdentificationType.size() != 0) {
                return new ResponseEntity<>(listIdentificationType, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(listIdentificationType, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }
}
