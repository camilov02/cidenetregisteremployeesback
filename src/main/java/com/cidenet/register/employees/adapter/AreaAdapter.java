package com.cidenet.register.employees.adapter;

import com.cidenet.register.employees.domain.entities.AreaEntity;
import com.cidenet.register.employees.domain.entities.IdentificationTypeEntity;
import com.cidenet.register.employees.repository.AreaRepository;
import com.cidenet.register.employees.services.AreaServicesImpl;
import com.cidenet.register.employees.services.IAreaServices;
import com.cidenet.register.employees.services.IIdentificationTypeServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/area")
public class AreaAdapter {

    @Autowired
    private IAreaServices areaServices;

    @GetMapping("/list")
    public ResponseEntity<?> listArea() {
        List<AreaEntity> listArea = areaServices.findAll();
        if (listArea != null) {
            if (listArea.size() != 0) {
                return new ResponseEntity<>(listArea, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(listArea, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }
}
