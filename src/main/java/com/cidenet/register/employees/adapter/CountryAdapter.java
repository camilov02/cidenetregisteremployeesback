package com.cidenet.register.employees.adapter;

import com.cidenet.register.employees.domain.entities.CountryEntity;
import com.cidenet.register.employees.services.ICountryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/country")
public class CountryAdapter {

    @Autowired
    private ICountryServices countryServices;

    @GetMapping("/list")
    public ResponseEntity<?> listCountry() {
        List<CountryEntity> listCountry = countryServices.findAll();
        if (listCountry != null) {
            if (listCountry.size() != 0) {
                return new ResponseEntity<>(listCountry, HttpStatus.OK);
            } else {
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }
}
