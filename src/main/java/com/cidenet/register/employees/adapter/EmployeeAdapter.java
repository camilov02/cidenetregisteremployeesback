package com.cidenet.register.employees.adapter;

import com.cidenet.register.employees.config.ApiError;
import com.cidenet.register.employees.domain.entities.*;
import com.cidenet.register.employees.domain.models.ApiResponseError;
import com.cidenet.register.employees.services.*;
import com.cidenet.register.employees.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@RestController
@RequestMapping("/api/employee")
public class EmployeeAdapter {

    @Autowired
    private IEmployeeServices employeeServices;

    @Autowired
    private IEmployeeByAreaServices employeeByAreaServices;

    @Autowired
    private ICountryServices countryServices;

    @Autowired
    private IAreaServices areaServices;

    @Autowired
    private IIdentificationTypeServices identificationTypeServices;

    private Utils utils = new Utils();

    @GetMapping("/list")
    public ResponseEntity<?> listEmployees() {
        List<EmployeeEntity> listEmployees = employeeServices.findAll();
        if (listEmployees != null) {
            if (listEmployees.size() != 0) {
                return new ResponseEntity<>(listEmployees, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(listEmployees, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<?> listEmployees(@Valid @RequestBody EmployeeEntity employeeRequest, Errors errors) {

        if(errors.hasErrors()) {
            List<String> listErrors = new ArrayList<>();
            for (ObjectError err: errors.getFieldErrors()) {
                String errorMessage = err.getDefaultMessage();
                listErrors.add(errorMessage);
            }
            return new ResponseEntity<>(listErrors, HttpStatus.BAD_REQUEST);
        }else {
            return RegisterEmployee(employeeRequest, true, false);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> listEmployees(@Valid @RequestBody EmployeeEntity employeeRequest, Errors errors, @PathVariable(value = "id") Long id) {

        if(errors.hasErrors()) {
            List<String> listErrors = new ArrayList<>();
            for (ObjectError err: errors.getFieldErrors()) {
                String errorMessage = err.getDefaultMessage();
                listErrors.add(errorMessage);
            }
            return new ResponseEntity<>(listErrors, HttpStatus.BAD_REQUEST);
        }else {
            Optional<EmployeeEntity> employeeEntity = employeeServices.findById(id);
            if(employeeEntity.isPresent()) {
                EmployeeEntity employeeSave = new EmployeeEntity();
                employeeSave = employeeRequest;

                if(employeeRequest.getIdentification().equals(employeeEntity.get().getIdentification())) {
                    return RegisterEmployee(employeeSave, false, true);
                }else {
                    return RegisterEmployee(employeeSave, true, true);
                }
            }else {
                return new ResponseEntity<>("El empleado no existe.", HttpStatus.BAD_REQUEST);
            }
        }
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") Long id) {

        Optional<EmployeeEntity> employeeEntity = employeeServices.findById(id);

        if (employeeEntity.isPresent()) {
            employeeServices.deleteEmployee(id);
            employeeByAreaServices.deleteById(employeeEntity.get().getEmployeeArea().getId());

            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>("El empleado no existe.", HttpStatus.BAD_REQUEST);
        }
    }

    private ResponseEntity<?> RegisterEmployee(EmployeeEntity employeeRequest, boolean isVerifyIdentification, boolean isModify) {
        EmployeeEntity employee = employeeServices.findByIdentification(employeeRequest.getIdentification());

        if(employee != null && isVerifyIdentification) {
            return  new ResponseEntity<>("El empleado ya se encuentra registrado", HttpStatus.BAD_REQUEST);
        }else {
            try {

                Optional<CountryEntity> countryEntity = countryServices.findById(employeeRequest.getCountry().getId());

                if(countryEntity.isPresent()) {

                    String emailGenerate = utils.GenerateEmail(employeeRequest, countryEntity.get().getDomain());
                    String emailSplit = utils.SplitEmailEmployee(emailGenerate);
                    employee = employeeServices.findEmployeeByEmailTop(emailSplit);

                    if(employee != null) {
                        emailGenerate = utils.BuildEmployeeEmailConsecutive(employee, countryEntity.get().getDomain());
                    }

                    employeeRequest.setEmail(emailGenerate);

                    return SaveEmployeeRequest(employeeRequest, isModify);
                }else {
                    return new ResponseEntity<>("No se encontró un país con el codigo especificado.", HttpStatus.BAD_REQUEST);
                }

            }catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }
    }

    private ResponseEntity<?> SaveEmployeeRequest(EmployeeEntity employeeRequest, boolean isModify) {

        EmployeeByAreaEntity employeeByAreaUpdate = new EmployeeByAreaEntity();
        EmployeeEntity employeeEntityUpdate = new EmployeeEntity();

        if(!isModify) {
            employeeByAreaServices.save(employeeRequest.getEmployeeArea());
            employeeServices.save(employeeRequest);

            return new ResponseEntity<>(HttpStatus.CREATED);
        }else {
            if(employeeRequest.getEmployeeArea().getId() != null) {
                Optional<EmployeeByAreaEntity> employee = employeeByAreaServices.findById(employeeRequest.getEmployeeArea().getId());
                Optional<AreaEntity> area = areaServices.findById(employeeRequest.getEmployeeArea().getArea().getId());
                Optional<IdentificationTypeEntity> identificationType = identificationTypeServices.findById(employeeRequest.getIdentificationType().getId());
                Optional<CountryEntity> country = countryServices.findById(employeeRequest.getCountry().getId());

                if(employee.isPresent() && area.isPresent()) {

                    employeeByAreaUpdate = employeeRequest.getEmployeeArea();
                    employeeByAreaUpdate.setCreateAt(employee.get().getCreateAt());
                    employeeByAreaUpdate.setState(true);
                    employeeByAreaUpdate.setArea(area.get());

                    employeeEntityUpdate.setId(employeeRequest.getId());
                    employeeEntityUpdate.setFirstLastName(employeeRequest.getFirstLastName());
                    employeeEntityUpdate.setSecondLastName(employeeRequest.getSecondLastName());
                    employeeEntityUpdate.setFirstName(employeeRequest.getFirstName());
                    employeeEntityUpdate.setSecondName(employeeRequest.getSecondName());
                    employeeEntityUpdate.setEmail(employeeRequest.getEmail());
                    employeeEntityUpdate.setIdentification(employeeRequest.getIdentification());

                    employeeEntityUpdate.setCountry(country.get());
                    employeeEntityUpdate.setIdentificationType(identificationType.get());
                    employeeEntityUpdate.setEmployeeArea(employeeByAreaUpdate);

                    employeeByAreaServices.save(employeeByAreaUpdate);
                    employeeServices.save(employeeEntityUpdate);

                    return new ResponseEntity<>(HttpStatus.OK);
                }else {
                    return new ResponseEntity<>("El id del area es obligatorio", HttpStatus.BAD_REQUEST);
                }
            }else {
                return new ResponseEntity<>("El id del area es obligatorio", HttpStatus.BAD_REQUEST);
            }
        }
    }
}
