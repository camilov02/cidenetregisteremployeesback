package com.cidenet.register.employees.utils;

import com.cidenet.register.employees.domain.entities.EmployeeEntity;

public class Utils {

    private String CheckCompound(String firstLastname) {
        String[] parts = firstLastname.split(" ");

        if(parts.length > 1) {
            return  firstLastname.replaceAll(" ","").toLowerCase();
        }else {
            return null;
        }
    }

    public String GenerateEmail(EmployeeEntity employeeEntity, String domain) {

        String compound = CheckCompound(employeeEntity.getFirstLastName().toLowerCase());
        String name = employeeEntity.getFirstName().toLowerCase();
        String lastname = compound != null ? compound : employeeEntity.getFirstLastName().toLowerCase();

        return name + "." + lastname + "@" +
                    domain;
    }

    public String SplitEmailEmployee(String email) {
        return email.split("\\@")[0];
    }

    public String BuildEmployeeEmailConsecutive(EmployeeEntity employee, String domain) {

        String name = employee.getFirstName().toLowerCase();
        String compound = CheckCompound(employee.getFirstLastName().toLowerCase());
        String lastname = compound != null ? compound : employee.getFirstLastName().toLowerCase();

        String emailGenerate = "";
        int consecutive = ValidateNumberInString(employee.getEmail());

        if (consecutive > 0) {
            consecutive = consecutive + 1;
            emailGenerate = name + "." + lastname + "." +
                    consecutive + "@" + domain;

        } else {
            emailGenerate = name + "." + lastname + "." + 1 +
                    "@" + domain;
        }
        return emailGenerate;
    }

    private int ValidateNumberInString(String email) {

        int count = 0;

        char[] chars = email.toCharArray();
        StringBuilder sb = new StringBuilder();
        for(char c : chars){
            if(Character.isDigit(c)){
                count++;
                sb.append(c);
            }
        }

        return count > 0 ? Integer.parseInt(sb.toString()) : 0;
    }
}
