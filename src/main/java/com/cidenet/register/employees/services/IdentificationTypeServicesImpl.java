package com.cidenet.register.employees.services;

import com.cidenet.register.employees.domain.entities.IdentificationTypeEntity;
import com.cidenet.register.employees.repository.IdentificationTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional()
public class IdentificationTypeServicesImpl implements IIdentificationTypeServices{

    @Autowired
    private IdentificationTypeRepository identificationTypeRepository;

    @Override
    public List<IdentificationTypeEntity> findAll() {
        return identificationTypeRepository.findAll();
    }

    @Override
    public Optional<IdentificationTypeEntity> findById(Long id) {
        return identificationTypeRepository.findById(id);
    }
}
