package com.cidenet.register.employees.services;

import com.cidenet.register.employees.repository.CountryRepository;
import com.cidenet.register.employees.domain.entities.CountryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CountryServicesImpl implements ICountryServices {

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public List<CountryEntity> findAll() {
        return countryRepository.findAll();
    }

    @Override
    public Optional<CountryEntity> findById(Long id) {
        return countryRepository.findById(id);
    }
}
