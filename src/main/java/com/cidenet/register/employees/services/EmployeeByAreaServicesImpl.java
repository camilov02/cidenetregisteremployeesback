package com.cidenet.register.employees.services;

import com.cidenet.register.employees.repository.EmployeeByAreaRepository;
import com.cidenet.register.employees.domain.entities.EmployeeByAreaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional()
public class EmployeeByAreaServicesImpl implements IEmployeeByAreaServices {

    @Autowired
    private EmployeeByAreaRepository employeeByAreaRepository;

    @Override
    public EmployeeByAreaEntity save(EmployeeByAreaEntity employeeByArea) {
        return employeeByAreaRepository.save(employeeByArea);
    }

    @Override
    public void deleteById(Long id) {
        employeeByAreaRepository.deleteById(id);
    }

    @Override
    public Optional<EmployeeByAreaEntity> findById(Long id) {
        return employeeByAreaRepository.findById(id);
    }
}
