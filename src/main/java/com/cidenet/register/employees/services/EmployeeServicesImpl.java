package com.cidenet.register.employees.services;

import com.cidenet.register.employees.repository.EmployeeRepository;
import com.cidenet.register.employees.domain.entities.EmployeeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EmployeeServicesImpl implements IEmployeeServices {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<EmployeeEntity> findAll() {
        return employeeRepository.findAllByOrderByIdDesc();
    }

    @Override
    public EmployeeEntity findByIdentification(String identification) {
        return employeeRepository.findByIdentification(identification);
    }

    @Override
    public void save(EmployeeEntity employee) {
        employeeRepository.save(employee);
    }

    @Override
    public EmployeeEntity findByEmail(String email) {
        return employeeRepository.findByEmail(email);
    }

    @Override
    public EmployeeEntity findEmployeeByEmailTop(String email) {
        return employeeRepository.findEmployeeByEmailTop(email);
    }

    @Override
    public Optional<EmployeeEntity> findById(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }
}
