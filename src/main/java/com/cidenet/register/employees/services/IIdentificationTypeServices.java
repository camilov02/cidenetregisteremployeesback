package com.cidenet.register.employees.services;

import com.cidenet.register.employees.domain.entities.IdentificationTypeEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface IIdentificationTypeServices {

    public List<IdentificationTypeEntity> findAll();
    public Optional<IdentificationTypeEntity> findById(Long id);
}
