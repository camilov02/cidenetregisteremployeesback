package com.cidenet.register.employees.services;

import com.cidenet.register.employees.domain.entities.AreaEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface IAreaServices {

    public List<AreaEntity> findAll();
    public Optional<AreaEntity> findById(Long id);
}
