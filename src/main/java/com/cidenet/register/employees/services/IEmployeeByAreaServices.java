package com.cidenet.register.employees.services;

import com.cidenet.register.employees.domain.entities.EmployeeByAreaEntity;

import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

@Transactional
public interface IEmployeeByAreaServices {

    public EmployeeByAreaEntity save(EmployeeByAreaEntity employeeByArea);
    public void deleteById(Long id);
    public Optional<EmployeeByAreaEntity> findById(Long id);
}
