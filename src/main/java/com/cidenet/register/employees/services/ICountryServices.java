package com.cidenet.register.employees.services;

import com.cidenet.register.employees.domain.entities.CountryEntity;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface ICountryServices {

    public List<CountryEntity> findAll();
    public Optional<CountryEntity> findById(Long id);

}
