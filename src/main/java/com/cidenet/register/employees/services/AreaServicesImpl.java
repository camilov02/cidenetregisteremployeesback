package com.cidenet.register.employees.services;

import com.cidenet.register.employees.domain.entities.AreaEntity;
import com.cidenet.register.employees.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AreaServicesImpl implements IAreaServices {

    @Autowired
    private AreaRepository areaRepository;

    @Override
    public List<AreaEntity> findAll() {
        return areaRepository.findAll();
    }

    @Override
    public Optional<AreaEntity> findById(Long id) {
        return areaRepository.findById(id);
    }
}
