package com.cidenet.register.employees.services;

import com.cidenet.register.employees.domain.entities.CountryEntity;
import com.cidenet.register.employees.domain.entities.EmployeeEntity;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface IEmployeeServices {

    public List<EmployeeEntity> findAll();
    public EmployeeEntity findByIdentification(String identification);
    public void save(EmployeeEntity employee);
    public EmployeeEntity findByEmail(String email);
    public EmployeeEntity findEmployeeByEmailTop(String email);
    public Optional<EmployeeEntity> findById(Long id);
    public void deleteEmployee(Long id);
}
